# -*- coding: utf-8 -*-

"""
Käesolev skript on loodud Gaussi protsesside mudelite treenimiseks, testimiseks ja andmete salvestamiseks faili. Siin on implementeeritud
funktsioonid sisend- ja väljundtunnuste kombineerimiseks, usaldusvahemiku leidmiseks ning usaldusvahemiku testimiseks.

Mõistlikum on mitte jooksutada tervet skripti vaid valida käsureaargumentide abil üks andmestik ja treeningsuurus korraga.

Käsureaargumente saab sisestada kujul argument=väärtus

train=treeningsuurus, täisarv; saab kasutada mitme treeningsuuruse jaoks, kui lisada korduvalt: train=1000 train=2500
label=märgend
data=andmestik ({wine, bike, boat]); mõistab ka eestikeelseid sisendeid (ratta,laeva,veini)
iters=iteratsioonide arv
feature=sisendtunnus; saab lisada mitu: feature=hr feature=mnth

"""

import random
import sys
from time import localtime, strftime

import scipy.stats
import sklearn.gaussian_process as GP
from mlxtend.preprocessing import standardize
from pandas import read_csv

#indikaatorfunktsioon, mis tagastab binaarse vektori ning ühtlasi ka kogu testsuuruse ning positiivsete tulemuste arvu
def correct_in_intervals(lower, upper, labels):
    h = len(labels)
    countvector = []
    count = 0
    for i in range(h):
        if (labels[i] < upper[i][0] and labels[i] > lower[i][0]):
            countvector.append(1)
            count += 1
        else:
            countvector.append(0)
    return h, count, countvector


#intervallide arvutamine ning kontrollimine
def interval_checking(testx, testy, model):
    mean, std = model.predict(testx, return_std=True)
    low, up = mean - 1.96 * std, mean + 1.96 * std
    return correct_in_intervals(low, up, testy)


#vaikimisi valikud
datafiles = ["winequality-white",'boatdata','bikehour']

#andmestike asukoht
datafolder = "~/Documents/bakatöö/code/data/"

#tulemuste salvestamise asukoht
targetfolder = "test/"

lower = 2.5
upper = 97.5

traindatasizes = []

#laevaandmestiku tunnused
boatattributes=[]
boatlabels = ['lp','v', 'GTT', 'GTn', 'Ggn', 'Ts', 'Tp', 'T48', 'T1', 'T2', 'P48', 'P1', 'P2', 'Pexh', 'TIC', 'mf']
#boatlabels = ['Compressor_coefficient']#,'Turbine_coefficient']

#rattaandmestiku tunnused
bikehourattributes = ["season", "yr", "mnth", "hr", "holiday", "weekday", "workingday", "weathersit", "temp", "atemp", "hum", "windspeed"]
bikehourlabels = ["cnt", "casual", "registered"]

#veiniandmestiku tunnused
winattributes = ["fixed_acidity","volatile_acidity","citric_acid","residual_sugar","chlorides","free_sulfur_dioxide","total_sulfur_dioxide","density","pH","sulphates","alcohol"]
winelabels = ["quality"]

argfeatures = []
arglabel = ""
iters = 100
# käsurea argumentide parsimine
if (len(sys.argv) > 1):
    for i in range(1, len(sys.argv)):
        opt = sys.argv[i].split("=")
        optname = opt[0]
        optval = opt[1]
        if "data" in optname:
            if "day" in optval:
                datafiles = ["bikeday"]
            elif "bike" in optval or "ratta" in optval or "ratas" in optval:
                datafiles = ["bikehour"]
            elif "boat" in optval or "paadi" in optval or "laev" in optval:
                datafiles = ["boatdata"]
            elif "wine" in optval or "vein" in optval:
                datafiles = ["winequality-white"]
        elif "train" in optname:
            traindatasizes = [int(optval)]
        elif "iter" in optname:
            iters = int(optval)
        elif "feature" in optname:
            argfeatures.append(optval)
        elif "label" in optname:
            arglabel = optval

if len(traindatasizes) == 0:
    traindatasizes = [1000]

class TrainInstance():
    def __init__(self, train_features, label):
        self.train_features = train_features
        self.label = label

    def __str__(self) -> str:
        return "label:" + self.label + "; features:" + str(self.train_features)

    def __repr__(self) -> str:
        return "label:" + self.label + "; features:" + str(self.train_features)


#sisend- ja väljundtunnuste kombinatsioonide leidmine
def feature_list(datafile):
    combs = []
    if datafile.startswith("wine"): #veiniandmestik
        labels = winelabels
        attributes = winattributes
    elif datafile.startswith("bike"): #rattaandmestik
        labels = bikehourlabels
        attributes = bikehourattributes
    else : #paadiandmestik
        labels = boatlabels
        attributes = boatattributes
    for label in labels: #kombineerimine
        otherlabels = labels[:]
        otherlabels.remove(label)
        possiblefeatures = attributes[:]
        possiblefeatures.extend(otherlabels)
        combs.append(TrainInstance(label=label, train_features=possiblefeatures))
        otherfeatures = possiblefeatures[:]
        for feat in possiblefeatures:
            combs.append(TrainInstance(label=label, train_features=[feat]))
            otherfeatures.remove(feat)
            for feature in otherfeatures:
                pair = [feat, feature]
                combs.append(TrainInstance(label=label, train_features=pair))
    return combs



#korduv testimine
for i in range(iters):
    random.seed(i) #et garanteerida erinevad treeningandmed juhuslikult võttes
    print("iteration", i)
    for trainsize in traindatasizes:
        for datafile in datafiles:
            #separator tähistab csv failis eraldavat märki
            if datafile.startswith("bike"):
                separator = ","
            elif datafile.startswith("wine"):
                separator = ";"
            else:
                separator = "\t"
            #iga kombinatsiooni proovitakse eraldi
            for combination in feature_list(datafile):
                label = combination.label
                features = combination.train_features
                cols = features[:]
                cols.append(label)
                df = read_csv(datafolder + datafile + '.csv', sep=separator, header=0, usecols=cols, squeeze=True)
                Y = standardize(df, columns=[label])
                X = df[features]

                #andmete indeksite loomine
                indexes = list(range(0, len(X)))

                #indeksid segatakse juhuslikku järjekorda
                random.shuffle(indexes)

                #võetakse esimesed indeksid järjekorras treeningsuuruse väärtuses
                trainindexes = indexes[0:trainsize]

                #võetakse treeningandmed valitud indeksitel
                trainx = X.loc[trainindexes]
                trainy = (Y.as_matrix().take(trainindexes)).reshape(len(trainindexes), 1)


                #ülejäänud indeksid on testimiseks
                alltestindexes = [i for i in indexes if not i in trainindexes]

                print("training model")
                #treenitakse mudel
                kernel = GP.kernels.RBF() + GP.kernels.WhiteKernel(noise_level=1)  # default almost
                GPm = GP.GaussianProcessRegressor(n_restarts_optimizer=10,
                                                  kernel=kernel)  # kui mitte määrata käsitsi kernelit, siis kasutab vaikimisi fikseeritud väärtustega kernelit, mille puhul optimeerimine ei aita
                GPm.fit(trainx, trainy)

                print("calculating binary vector")

                #võetakse testandmed
                testx = X.loc[alltestindexes]
                testy = Y.as_matrix().take(alltestindexes)

                #leitakse binaarne vektor
                kokku, pos, vec = interval_checking(testx, testy, GPm)
                print("binary vector calculated")

                #arvutatakse p-väärtused vektorist
                greater = scipy.stats.binom_test(pos, kokku, 0.95, alternative='greater')
                less = scipy.stats.binom_test(pos, kokku, 0.95, alternative='less')
                biased = scipy.stats.binom_test(pos, kokku, 0.95, alternative='two-sided')

                #igale testimisele genereeritakse unikaalne failinimi, kuhu salvestada tulemused
                t = strftime("%H%M%S%s", localtime())
                filename = datafile + '-feature' + "".join(features) + '-label' + label + '-train' + str(
                    trainsize) + '-' + t
                filename += ".csv"

                #tulemused salvestatakse faili
                with open(targetfolder + filename, mode='w+', encoding='utf-8') as f:
                    f.write(
                        "greater:" + str(greater) + ";less:" + str(less) + ";twosided:" + str(biased) + ";upper:" + str(
                            upper) + ";testsize:" + str(kokku) + ";inbounds:" + str(pos) + ";lower:" + str(
                            lower) + ";features:" + ",".join(
                            features) + ";featuresnr:" + str(len(features)) + ";label:" + label + ";trainsize:" + str(
                            trainsize) + "\n")
                    #salvestab iga testandme väärtuse ja selle, kas ta kuulus usaldusvahemikku
                    if len(features) == 1:
                        for i in range(len(vec)):
                            f.write(str(testx.iloc[i][features[0]]) + ";" + str(vec[i]) + "\n")
