"""
Tegemist on skriptiga andmete visualiseerimiseks. See on mõeldud IDE-s käivitamiseks ja jooksvalt muutmiseks,
tegemist on lihtsalt matplotlib.pyplot teegi kasutamisega.
"""


from pandas import read_csv
import matplotlib.pyplot as plt
import pandas as pd


df = read_csv('results.csv', sep=";", header=0, squeeze=True)

def plot_perc(df,save = False, ext = "", roundv = 1):
    width = 0.9

    minval = 0.945
    maxval = 0.955

    values = [0,0.905,0.91,0.915,0.92,0.925,0.93,0.935,0.94,minval,0.95,maxval,0.96,0.965,0.97,0.975,0.98,0.985,0.99,0.995,1.]
    colors = []
    for i in range(len(values)):
        val = values[i]
        if val <minval:
            colors.append("#FF5733")#, "#DAF7A6", "#FFC300")
        elif val >=maxval:
            colors.append("#FFC300")
        else:
            colors.append("#DAF7A6")
    #ticks = ["< " + str(min_val), str(min_val) + " ≤ ... ≤ " + str(max_val), str(max_val) + " <"]
    ticks = []
    firstval = 0
    ticks.append("0")
    for i in range(len(values)):
        ticks.append(str(values[i]))#+"..."+str(values[i+1]))

    out = pd.cut(df["perc"], bins=values)
    out_norm = out.value_counts(sort=False, normalize=True).mul(100)
    ax = out_norm.plot.bar(rot=0, width=width,color=colors, edgecolor=colors,figsize=(10, 6))

    for p in ax.patches:
        ax.annotate(str(round(p.get_height(),roundv)), (p.get_x() * 1.005, p.get_height() * 1.005))

    ax.xaxis.set_ticks_position('none')

    plt.plot([9.5,9.5],[0,16],'g-', label="0.95",lw=2)
    plt.text(9.5,15, "0.95")

    plt.ylabel("Mudeleid (%)")
    plt.xlabel("Osakaal märgenditest usaldusvahemikus")

    plt.xticks(rotation=70)
    plt.tight_layout()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    #ax.set_xticklabels([c[1:-1].replace(","," to") for c in out.cat.categories])
    if save:
        plt.savefig(ext+"all_results_bins.png")
    plt.show()


def plot_pvalue(df, save = False, ext = ""):
    width = 0.9

    minval = 0.01
    maxval = 1-minval

    values = [0,minval,maxval,1]
    colors = []
    for i in range(len(values)):
        val = values[i]
        if val <minval:
            colors.append("#FF5733")#, "#DAF7A6", "#FFC300")
        elif val >=maxval:
            colors.append("#FFC300")
        else:
            colors.append("#DAF7A6")
    ticks = []
    ticks.append("0")
    for i in range(len(values)):
        ticks.append(str(values[i]))#+"..."+str(values[i+1]))

    out = pd.cut(df["lessp"], bins=values,include_lowest=True)
    out_norm = out.value_counts(sort=False, normalize=True).mul(100)

    ax = out_norm.plot.bar(rot=0, width=width,color=colors, edgecolor=colors,figsize=(9, 3.5))
    print(out.cat.categories)
    labels = ["Väiksem tulemus kui 0,95 on oluline", "Erinevus 0,95-st on ebaoluline", "Suurem tulemus kui 0,95 on oluline"]
    ax.set_xticklabels(labels)

    for p in ax.patches:
        ax.annotate(str(round(p.get_height(),1)), (p.get_x()+0.5*width-0.05, p.get_height() * 1.005))

    ax.xaxis.set_ticks_position('none')

    plt.ylabel("Mudeleid (%)")
    plt.xlabel("Erinevuse olulisus väärtusest 0,95 usaldusnivool "+str(minval))

    plt.xticks(rotation=15)
    plt.tight_layout()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    #ax.set_xticklabels([c[1:-1].replace(","," to") for c in out.cat.categories])
    if save:
        plt.savefig(ext+"all_pvalues.png")
    plt.show()

#plot_pvalue(df)#, True)
#
def plot_perc_forallfeatures(df, save = False, ext = ""):
    width = 0.9

    minval = 0.94
    maxval = 0.96

    values = [0,0.9,0.92,minval,maxval,0.98,1.]
    colors = []
    for i in range(len(values)):
        val = values[i]
        if val <minval:
            colors.append("#FF5733")#, "#DAF7A6", "#FFC300")
        elif val >=maxval:
            colors.append("#FFC300")
        else:
            colors.append("#DAF7A6")
    #ticks = ["< " + str(min_val), str(min_val) + " ≤ ... ≤ " + str(max_val), str(max_val) + " <"]
    ticks = []
    firstval = 0
    ticks.append("0")
    for i in range(len(values)):
        ticks.append(str(values[i]))#+"..."+str(values[i+1]))

    colours = {1: 'r', 2: 'b', 3: 'g'}

    out = pd.cut(df["perc"], bins=values)
    out_norm = out.value_counts(sort=False, normalize=True).mul(100)
    ax = out_norm.plot.bar(rot=0, width=width,color=colors, edgecolor=colors,figsize=(10, 6))

    for p in ax.patches:
        ax.annotate(str(round(p.get_height(),1)), (p.get_x()+0.5*width-0.1, p.get_height() * 1.005))

    ax.xaxis.set_ticks_position('none')

    #plt.plot([9.5,9.5],[0,16],'g-', label="0.95",lw=2)
    #plt.text(9.5,15, "0.95")

    plt.ylabel("Mudeleid (%)")
    plt.xlabel("Osakaal märgenditest usaldusvahemikus")

    plt.xticks(rotation=70)
    plt.tight_layout()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    #ax.set_xticklabels([c[1:-1].replace(","," to") for c in out.cat.categories])
    if save:
        plt.savefig(ext+"all_results_bins.png")
    plt.show()


#oneFeature = df.loc[df['featuresnr'] == 2]
#wineone = oneFeature.loc[oneFeature["dataset"]=='wine']
#plot_perc(wineone, ext="onefeature",roundv=2)

#twoFeature = df.loc[df['featuresnr'] == 2]
#plot_perc(twoFeature, ext="twofeature")

#moreFeature = df.loc[df['featuresnr'] > 2]
#plot_perc_forallfeatures(moreFeature, ext="allfeatures",save=True)








