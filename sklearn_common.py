"""

Antud skript on loodud eesmärgiga kõikide katsete loodud andmefailidest luua ülevaatlik andmefail. Olemas on ka meetod,
mis loob faili selleks, et tulemusi vahemike kaupa uurida. Selles andmefailis on eraldi veerud iga vahemiku kohta.

Sisendiks on oodatud sklearn_script.py väljundina loodud .csv failid. Need failid peaksid asuma argumendina antud kaustas.

Käsurea argumentidega saab määrata sisendfailide kausta ning väljundfaili kausta ja nime, ühtlasi kas on soov vahemike kaupa või mitte andmeid töödelda.

Argumendid:
source=sisendkaust
savedir=salvestatav kaust
savefile=väljundfaili nimi
groups (lisada ainult siis, kui on soov vahemike kaupa töödelda)


"""

import os
import sys

import numpy as np
import scipy.stats

#klass ühe tulemuse hoiustamiseks
class Result:
    def __init__(self, label,trainsize,inbounds,less, greater,twosided, testsize,filename,upper=97.5, lower=2.5, features=None, values=None):
        if features is None:
            features = []
        if values is None:
            values = []
        self.lower = lower
        self.upper = upper
        self.trainsize = trainsize
        self.label = label
        self.features = features
        self.filename = filename
        self.testsize = testsize
        self.greater = greater
        self.less = less
        self.twosided = twosided
        self.values = values
        self.inbounds = inbounds

    def __str__(self):
        return "train:" + str(self.trainsize) + " features:" + str(self.features )+ ";" + self.filename[0:4]

pidevad = ["temp","atemp",	"v","hum","windspeed","GTT","GTn","Ggn","Ts","Tp","T48","T1","T2","P48","P1","P2","Pexh","TIC","mf"]

sourcedirectory = "sklearnresults/"
savedirectory = "testresults/"
targetfile = None
groups = False

# käsurea argumentide parsimine
if (len(sys.argv) > 1):
    for i in range(1, len(sys.argv)):
        opt = sys.argv[i].split("=")
        optname = opt[0]
        optval = opt[1]
        if "source" in optname:
            sourcedirectory = optval
        elif "savedir" in optname:
            savedirectory = optval
        elif "savefile" in optname:
            targetfile = optval
        elif "group" in optname:
            groups = True

def get_results(save=False, targetfile = "results_test.csv"):
    results = []
    with open(targetfile, 'w') as resultsfile:
        if save:
            resultsfile.write("label;features;featuresnr;lessp;greaterp;twosidesp;perc;testsize;inbounds;trainsize;effect;dataset\n")
        for filename in os.listdir(sourcedirectory):
            if filename.endswith(".csv"):
                data = filename[0:4]
                with open(sourcedirectory + filename) as f:
                    lines = f.readlines()
                    opts = lines[0].split(";")
                    featuresnr = 0
                    for opt in opts:
                        optmap = opt.split(":")
                        optname = optmap[0].strip()
                        optval = optmap[1].strip()
                        if ("label" in optname):
                            label = optval
                        elif ("greater" in optname):
                            greater = float(optval)
                        elif ("less" in optname):
                            less = float(optval)
                        elif ("twosided" in optname):
                            twosided = float(optval)
                        elif ("testsize" in optname):
                            testsize = int(optval)
                        elif("inbounds" in optname):
                            inbounds = int(optval)
                        elif("nr" in optname and "feature" in optname):
                            featuresnr=int(optval)
                        elif("features" in optname):
                            features = optval.split(",")
                        elif("trainsize" in optname):
                            trainsize = int(optval)
                    if featuresnr == 1:
                        if features[0] in pidevad or data == "wine":
                            vals = [i.strip().split(";") for i in lines[1:]]
                            values = []
                            for val in vals:
                                featval = float(val[0])
                                binval = int(val[1])
                                values.append((featval,binval))
                            results.append(Result(label=label, greater=greater,
                                                  less=less, twosided=twosided, testsize=testsize,
                                                  inbounds=inbounds, features=features, trainsize=trainsize, values=values, filename=filename))
                        else:
                            results.append(Result(label=label, greater=greater,
                                                  less=less, twosided=twosided, testsize=testsize,
                                                  inbounds=inbounds, features=features, trainsize=trainsize,
                                                  filename=filename))
                    else:
                        results.append(Result(label=label,greater=greater,
                                              less=less,twosided=twosided,testsize=testsize,
                                              inbounds=inbounds,features=features,trainsize=trainsize, filename=filename))
                    if save:
                        perc = inbounds * 1. / testsize * 1.
                        resultstring= label + ";" + ",".join(features) + ";" + str(len(features)) + ";" + str(less) + ";" +\
                                str(greater) + ";" + str(twosided) + ";" + str(perc) + ";" + str(testsize) + ";" +\
                                str(inbounds) + ";" + str(trainsize) + ";" + str(abs(0.95 - perc)) + ";" + data + "\n"
                        resultsfile.write(resultstring)

    return results

results = get_results()

#võtab ainult sellised Results ehk tulemuste objektid, mida on plaanis vahemike kaupa uurida
def save_results_with_groups(results, filename="results_val.csv"):
    with open(filename, 'w') as resultsfile:
        colnames= "label;features;featuresnr;"
        #loob iga vahemiku jaoks eraldi veerud andmete hoiustamiseks
        for i in range(10):
            colnames += "lessp_"+str(i)+";"
            colnames += "twosidesp_"+str(i)+";"
            colnames += "perc_"+str(i)+";"
            colnames += "testsize_"+str(i)+";"
            colnames += "inbounds_"+str(i)+";"
            colnames += "effect_"+str(i)+";"
        colnames += "trainsize;dataset\n"
        resultsfile.write(colnames)

        for res in results:
            if res.values:
                resultstring = res.label + ";" + ",".join(res.features) + ";" + str(len(res.features)) + ";"
                values = res.values[:]
                values.sort(key = lambda tup: tup[0])
                chunks = np.array_split(values,10)
                for i in range(10):
                    chunk = chunks[i]
                    total = len(chunk)
                    correct = 0
                    for pair in chunk:
                        if pair[1] == 1:
                            correct += 1
                    perc = correct*1./total*1.
                    less = scipy.stats.binom_test(correct, total, 0.95, alternative='less')
                    biased = scipy.stats.binom_test(correct, total, 0.95, alternative='two-sided')

                    resultstring+=str(less)+";"
                    resultstring+=str(biased)+";"
                    resultstring+=str(perc)+";"
                    resultstring+=str(total)+";"
                    resultstring+=str(correct)+";"
                    resultstring+=str(abs(perc-0.95))+";"
                resultstring+=str(res.trainsize)+";"
                resultstring+=res.filename[0:4]+"\n"

                resultsfile.write(resultstring)

if targetfile:
    if not groups:
        get_results(True, targetfile)
    else:
        save_results_with_groups(results,targetfile)
else:
    if groups:
        save_results_with_groups(get_results())
    else:
        get_results(True)

